## [1.4.2](https://gitlab.com/javi.ns/test-semantic-release/compare/v1.4.1...v1.4.2) (2021-03-09)


### Bug Fixes

* add changelog.md ([31c64b4](https://gitlab.com/javi.ns/test-semantic-release/commit/31c64b4f9d259c3ae566de4baa53195daa4a0e54))

# [1.4.0](https://gitlab.com/javi.ns/test-semantic-release/compare/v1.3.1...v1.4.0) (2021-03-09)


### Bug Fixes

* min ([c20e0c6](https://gitlab.com/javi.ns/test-semantic-release/commit/c20e0c6f14080bf951befb9c62c317907538004c))


### Features

* add version increment to .releaserc ([8e50277](https://gitlab.com/javi.ns/test-semantic-release/commit/8e50277305415b1e8789f664654c40f96239e80a))

## [1.3.1](https://gitlab.com/javi.ns/test-semantic-release/compare/v1.3.0...v1.3.1) (2021-03-09)


### Bug Fixes

* .releaserc ([7a2eb64](https://gitlab.com/javi.ns/test-semantic-release/commit/7a2eb64a5d256c41cf90e7b746ac68d0dfe6647f))
* remove test2 ([18aae2f](https://gitlab.com/javi.ns/test-semantic-release/commit/18aae2f51dda937cd2f1a8ef954693c2c5caccbe))
* remove test3 ([2a259dc](https://gitlab.com/javi.ns/test-semantic-release/commit/2a259dc11ab97ea04003bfa1ffc20c5e1860c726))
* test ([1fd5f46](https://gitlab.com/javi.ns/test-semantic-release/commit/1fd5f46198d8401e929cff375446ea24864ff96a))

# [1.3.0](https://gitlab.com/javi.ns/test-semantic-release/compare/v1.2.1...v1.3.0) (2021-02-26)


### Features

* aaaaa ([e671198](https://gitlab.com/javi.ns/test-semantic-release/commit/e67119855685d8fc945e1de74d35edd9a96a572a))

## [1.2.1](https://gitlab.com/javi.ns/test-semantic-release/compare/v1.2.0...v1.2.1) (2021-02-26)


### Bug Fixes

* fdfdsfs ([7a254d4](https://gitlab.com/javi.ns/test-semantic-release/commit/7a254d4e295d3b52defc6d13747a0c1463f4a4c4))

# [1.2.0](https://gitlab.com/javi.ns/test-semantic-release/compare/v1.1.0...v1.2.0) (2021-02-26)


### Bug Fixes

* **test.txt:** first minor test ([40f36c7](https://gitlab.com/javi.ns/test-semantic-release/commit/40f36c7321a29cd8b600eaa58ea28a5f8fb679d8))


### Features

* add feature ([0bb1195](https://gitlab.com/javi.ns/test-semantic-release/commit/0bb11952ea72c7d44a2aaef821ac2e37e0954c7d))

# [1.1.0](https://gitlab.com/javi.ns/test-semantic-release/compare/v1.0.0...v1.1.0) (2021-02-26)


### Features

* add main.c to project ([9204c09](https://gitlab.com/javi.ns/test-semantic-release/commit/9204c09f39d7eb1dd1ddf8b1b9d9c0da1c1bbe9e))

# 1.0.0 (2021-02-26)


### Bug Fixes

* **package.json:** replace domain .ns by .com ([566e60a](https://gitlab.com/javi.ns/test-semantic-release/commit/566e60abff3f5f9355ab2e137c38c4208b09b3b9))
* **package.json:** replace domain .ns by .com ([ebaa789](https://gitlab.com/javi.ns/test-semantic-release/commit/ebaa789de56fd6106cda441930e816ee88ae9958))


### Features

* release without npm ([301b85c](https://gitlab.com/javi.ns/test-semantic-release/commit/301b85ce89dae6ee1f9158e1cd1652d817af6867))
